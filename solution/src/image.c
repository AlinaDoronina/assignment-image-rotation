#include "../include/image.h"

#include <inttypes.h>
#include <stdlib.h>

struct image image_create(uint64_t width, uint64_t height) {
        return (struct image) {width, height, malloc(sizeof(struct pixel)*width*height)};
}

void image_free(struct image* img) {
        free(img->data);
}
