#include "../include/image.h"
#include "../include/transformations.h"
#include "../include/work_with_bmp.h"
#include "../include/work_with_files.h"
#include "../include/fopen_argument_enum.h"

#include <errno.h> 
#include <inttypes.h>
#include  <stdint.h>
#include <stdio.h>
#include <stdlib.h>

int main(int argcnt, char** files) {
  if (argcnt < 3) return 1;

  const char* path_in = files[1];
  const char* path_out = files[2];

  FILE* in = 0;
  FILE* out = 0;

  struct image* img = &(struct image){0, 0, 0};

  if (
    !open_file_handler(path_in, &in, R) ||
    !from_bmp_handler(&in, img) ||
    !close_file_handler(&in)
  ) return 1;

  struct image img_rot = rotate(*img);

  if (
    !open_file_handler(path_out, &out, W) ||
    !to_bmp_handler(&out, &img_rot) ||
    !close_file_handler(&out)
  ) return 1;

  image_free(img);
  image_free(&img_rot);

  return 0;
}
