#include <inttypes.h>

#ifndef IMAGE_H
#define IMAGE_H

    struct pixel { uint8_t b, g, r; };
    struct image {
        uint64_t width, height;
        struct pixel* data;
    };
    struct image image_create(uint64_t width, uint64_t height);
    void image_free(struct image* img);
    
#endif
