#ifndef WORK_WITH_BMP_H
#define WORK_WITH_BMP_H

#include "../include/image.h"
#include <stdio.h>
#include <stdbool.h>

    bool from_bmp_handler (FILE** in, struct image* img);
    bool to_bmp_handler (FILE** out, struct image const* img);

#endif
