#include "../include/work_with_bmp.h"
#include "../include/image.h"
#include "../include/read_enum.h"
#include "../include/write_enum.h"


#include <errno.h> 
#include <inttypes.h>
#include  <stdint.h>
#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>

//CONSTANSTS
uint16_t BMP_TYPE = 19778; uint32_t BMP_RESERVED = 0; uint32_t BMP_OFFBITS = 54;
uint32_t BMP_SIZE = 40; uint32_t BMP_PLANES = 1; uint32_t BMP_BIT_COUNT = 24;
uint32_t BMP_COMPRESSION = 0; uint32_t BMP_X_PELS_PER_METER = 2834; uint32_t BMP_Y_PELS_PER_METER = 2834;
uint32_t BMP_CRL_USED = 0; uint32_t BMP_CRL_IMPORTANT = 0;

struct __attribute__((packed)) bmp_header {
        uint16_t bfType;
        uint32_t  bfileSize;
        uint32_t bfReserved;
        uint32_t bOffBits;
        uint32_t biSize;
        uint32_t biWidth;
        uint32_t  biHeight;
        uint16_t  biPlanes;
        uint16_t biBitCount;
        uint32_t biCompression;
        uint32_t biSizeImage; 
        uint32_t biXPelsPerMeter;
        uint32_t biYPelsPerMeter;
        uint32_t biClrUsed;
        uint32_t  biClrImportant;
};

enum bmp_header_status {
  HEADER_OK,
  HEADER_INVALID_BITS,
  HEADER_EMPTY_FILE,
  HEADER_INVALID_TYPE,
  HEADER_INVALID_FILESIZE,
  HEADER_INVALID_RESERVED,
  HEADER_INVALID_PLANES,
  HEADER_INVALID_SIZE,
  HEADER_INVALID_BITS_COUNT,
  HEADER_INVALID_WIDTH,
  HEADER_INVALID_HEIGTH,
  HEADER_INVALID_OFFBITS
};

static uint32_t calculate_padding (struct image const* img) {
    if (img->width%4==0) return 0;
    else return 4-(3*img->width)%4;
}

void bmp_header_from_image(struct image const* img, struct bmp_header* bh) {

  const uint32_t padding = calculate_padding(img);

  *bh = (struct bmp_header) {
    .bfType = BMP_TYPE,
    .bfileSize = sizeof(struct bmp_header)+(img->height*img->width)*3+padding*img->height,
    .bfReserved = BMP_RESERVED, .bOffBits = BMP_OFFBITS, .biSize = BMP_SIZE,
    .biWidth = img->width, .biHeight = img->height,
    .biPlanes = BMP_PLANES, .biBitCount = BMP_BIT_COUNT, .biCompression = BMP_COMPRESSION,
    .biSizeImage = (img->height*img->width)*3+padding*img->height,
    .biXPelsPerMeter = BMP_X_PELS_PER_METER, .biYPelsPerMeter = BMP_Y_PELS_PER_METER, .biClrUsed = BMP_CRL_USED, .biClrImportant = BMP_CRL_IMPORTANT
  };

}

static enum write_status write_bmp_header( FILE** out, struct image const* img) {

  if (*out==NULL) return WRITE_EMPTY_FILE;

  struct bmp_header bh = {0};
  bmp_header_from_image(img, &bh);

  fwrite(&bh, sizeof(struct bmp_header), 1, *out);

  if (ferror(*out)) return WRITE_ERROR_FWRITE;

  return WRITE_OK;
}

static enum write_status write_picture( FILE** out, struct image const* img ) {
  if (*out==NULL) return WRITE_EMPTY_FILE;

  const uint32_t padding = calculate_padding(img);
  
  for (uint32_t i = 0; i <  img->height; i++) {
    for (uint32_t j = 0; j < img->width; j++) {

      fwrite(img->data+(img->height-1-i)*img->width+j, sizeof(struct pixel), 1, *out);
      if (ferror(*out)) return WRITE_ERROR_FWRITE;

    }

    fseek(*out, padding, SEEK_CUR);
    if (ferror(*out)) return WRITE_ERROR_FSEEK;
  }

  return WRITE_OK;
}

static enum write_status to_bmp( FILE** out, struct image const* img ) {
  if (*out==NULL) return WRITE_EMPTY_FILE;

  const enum write_status result_write_bmp_header = write_bmp_header(out, img);
  if (result_write_bmp_header>0) return result_write_bmp_header;

  const enum write_status result_write_picture = write_picture(out, img);
  if (result_write_picture>0) return result_write_picture;
  
  return WRITE_OK;
}

static enum bmp_header_status check_bmp_header(struct bmp_header bh) {
  if (bh.bfType!=0x4d42 && bh.bfType!=0x4349 && bh.bfType!=0x5450) return HEADER_INVALID_TYPE;
  if (bh.bOffBits != 14+bh.biSize) return HEADER_INVALID_OFFBITS;
  if (bh.bfReserved != 0 ) return HEADER_INVALID_RESERVED;
  if (bh.biPlanes   != 1) return HEADER_INVALID_PLANES;
  if (bh.biSize!=40 && bh.biSize!=108 && bh.biSize!=124) return HEADER_INVALID_SIZE;
  if (bh.biBitCount    != 24) return HEADER_INVALID_BITS_COUNT;
  if (bh.biWidth <1 || bh.biWidth >10000) return HEADER_INVALID_WIDTH;
  if (bh.biHeight<1 || bh.biHeight>10000) return HEADER_INVALID_HEIGTH;
  return HEADER_OK;
}

static enum read_status bmp_header_status_to_read_status(enum bmp_header_status hs) {
  switch (hs) {
    case HEADER_INVALID_TYPE: return READ_INVALID_TYPE;
    case HEADER_INVALID_OFFBITS: return READ_INVALID_OFFBITS;
    case HEADER_INVALID_RESERVED: return READ_INVALID_RESERVED;
    case HEADER_INVALID_PLANES: return READ_INVALID_PLANES;
    case HEADER_INVALID_SIZE: return READ_INVALID_SIZE;
    case HEADER_INVALID_BITS_COUNT: return READ_INVALID_BITS_COUNT;
    case HEADER_INVALID_WIDTH: return READ_INVALID_WIDTH;
    default: return READ_INVALID_HEIGTH;
  }
}

static enum read_status from_bmp(FILE** in, struct image* img) {
  struct bmp_header bh = {0};

  fread(&bh, sizeof(struct bmp_header), 1, *in);
  if (ferror(*in)) return READ_ERROR_FREAD;
  
  const enum bmp_header_status hs_result = check_bmp_header(bh);
  if (hs_result) return bmp_header_status_to_read_status(hs_result);

  const uint32_t width = bh.biWidth;
  const uint32_t height = bh.biHeight;

  struct image maybe_img = (struct image) {bh.biWidth, bh.biHeight, malloc(sizeof(struct pixel)*width*height)};
  const uint32_t padding = calculate_padding(&maybe_img);

  for (uint32_t i=0; i<height; i++) {
        for (uint32_t j=0; j<width; j++) {

          fread(maybe_img.data+(height-1-i)*width+j, sizeof(struct pixel), 1, *in);
          if (ferror(*in)) {
            image_free(&maybe_img);
            return READ_ERROR_FREAD;
          }
        }

        fseek(*in, padding, SEEK_CUR);
        if (ferror(*in)) {
          image_free(&maybe_img);
          return READ_ERROR_FSEEK;
        }
  }
  
  *img = maybe_img;
  return READ_OK;
}

bool from_bmp_handler (FILE** in, struct image* img) {

  const enum read_status result_from_bmp = from_bmp(in, img);
  if (result_from_bmp!=0) {
    fprintf(stderr, "File reading error. Error code: %d\n", result_from_bmp);
    return false;
  }
  else return true;
}

bool to_bmp_handler ( FILE** out, struct image const* img ) {

  const enum write_status result_to_bmp = to_bmp(out, img);
  if (result_to_bmp!=0) {
    fprintf(stderr, "File writing error. Error code: %d\n", result_to_bmp);
    return false;
  }
  else return true;
}
