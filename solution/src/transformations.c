#include "../include/transformations.h"

#include "../include/image.h"

#include <inttypes.h>

struct image rotate(struct image const source) {
  const uint32_t new_width = source.height; //gor
  const uint32_t new_height = source.width; //ver
  struct image img_rev = image_create(new_width, new_height);

  for (uint32_t i=0; i<new_height; i++) {
      for (uint32_t j=0; j<new_width; j++) {
          *(img_rev.data+(new_height-1-i)*new_width+j) = *(source.data+j*source.width+i);
      }
  }

  return img_rev;
}
