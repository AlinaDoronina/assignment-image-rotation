#include "../include/work_with_files.h"
#include "../include/fopen_argument_enum.h"
#include "../include/open_close_enum.h"

#include <errno.h>
#include <inttypes.h> 
#include  <stdint.h>
#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>

const char* const fopen_argiment_char[] = {
    [R] = "r",
    [W] = "w"
};

static enum open_status open_file (const char* path, FILE** file, enum fopen_argument fopen_argument_enum) {

  *file = fopen(path, fopen_argiment_char[fopen_argument_enum]);
  if (*file==NULL) return OPEN_ERROR;

  return OPEN_OK;
}

static enum close_status close_file (FILE** file) {
  const int result_fclose = fclose(*file);
  if (result_fclose!=0) return CLOSE_ERROR;
  return CLOSE_OK;
}

bool open_file_handler (const char* path, FILE** file, enum fopen_argument fopen_argument_enum) {

  const enum open_status result_open_file_read = open_file(path, file, fopen_argument_enum);
  if (result_open_file_read!=0) {
    fprintf(stderr, "File opening error. Error code: %d\n", result_open_file_read);
    return false;
  }
  else return true;
}

bool close_file_handler (FILE** file) {

  const enum close_status result_close_file = close_file(file);
  if (result_close_file!=0) {
    fprintf(stderr, "File closing error. Error code: %d\n", result_close_file);
    return false;
  }
  else return true;
}
