#ifndef WORK_WITH_FILES_H
#define WORK_WITH_FILES_H

#include "../include/fopen_argument_enum.h"

#include <stdio.h>
#include <stdbool.h>

    bool open_file_handler (const char* path, FILE** file, enum fopen_argument fopen_argument_enum);
    bool close_file_handler (FILE** file);

#endif
