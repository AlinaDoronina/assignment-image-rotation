#ifndef WRITE_ENUM_H
#define WRITE_ENUM_H

    enum  write_status  {
        WRITE_OK = 0,
        WRITE_ERROR_FWRITE,
        WRITE_EMPTY_FILE,
        WRITE_ERROR_FSEEK,
    };

#endif
